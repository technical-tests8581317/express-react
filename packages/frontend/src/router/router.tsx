import { createBrowserRouter } from 'react-router-dom';

import Root from '../pages/Root';
import Design from '../pages/Design';
import UrlsValidation from '../pages/UrlsValidation';
const router = createBrowserRouter([
  {
    path: '/',
    element: <Root />,
  },
  {
    path: '/design',
    element: <Design />,
  },
  {
    path: '/urls-validation',
    element: <UrlsValidation />,
  },
]);

export default router;

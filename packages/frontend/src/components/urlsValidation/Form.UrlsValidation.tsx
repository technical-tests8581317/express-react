import { useState } from 'react';

//Functions
import checkUrlFormat from '../../functions/checkUrlFormat';

//Components
import Alert from '../Alert';

//Axios
import axios from 'axios';

//Types
import ResponseModel from '../../../../core/models/Response.Model';
import NotificationModel from '../../../../core/models/Notification.Model';
interface FormUrlPropsModel {
  newUrl: () => void;
}

export default function FormUrl(props: FormUrlPropsModel) {
  const [url, setUrl] = useState<string>('');
  const [notification, SetNotification] = useState<NotificationModel>({ status: '', message: '' });

  const handleChange = async (e: React.FormEvent<HTMLInputElement>) => {
    const target = e.target as HTMLButtonElement;
    const url: string = target.value;
    SetNotification({ status: '', message: '' });
    setUrl(url);
  };

  const addUrl = async (url: string) => {
    try {
      const response: ResponseModel = await axios.post(`${process.env.REACT_APP_API_URL}/checkUrl`, { url }, {});
      if (response.data.ok) {
        props.newUrl();
        SetNotification({ status: 'success', message: 'Url Added with success' });
      } else {
        SetNotification({ status: 'error', message: response.data.error });
      }
    } catch (error) {
      SetNotification({ status: 'error', message: 'Something wrong happened' });
    }
  };

  const checkUrl = async () => {
    const [errorUrl, dataUrl] = checkUrlFormat(url);
    if (errorUrl && !dataUrl) {
      SetNotification({ status: 'error', message: errorUrl.message });
    } else {
      await addUrl(url);
    }
  };

  const submitForm = async (e: React.MouseEvent<HTMLElement>) => {
    e.preventDefault();
    checkUrl();
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    checkUrl();
  };

  return (
    <div id='formUrlValidation'>
      <form onSubmit={handleSubmit} className='space-y-8 divide-y divide-gray-200'>
        <div className='space-y-8 divide-y divide-gray-200 sm:space-y-5'>
          <div className='space-y-6 sm:space-y-5'>
            <div>
              <h2 className='text-base font-semibold leading-6 text-gray-900'>Form</h2>
              <p className='mt-1 text-sm text-gray-500'>
                Please enter an url and we will check the security of your website. The list of your urls will be shown under the form.
              </p>
            </div>

            <div className='space-y-6 sm:space-y-5'>
              <div className='sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:border-t sm:border-gray-200 sm:pt-5'>
                <label htmlFor='url' className='block text-sm font-medium leading-6 text-gray-900 sm:pt-1.5'>
                  Enter Your Url
                </label>
                <div className='mt-2 sm:col-span-2 sm:mt-0'>
                  <div className='flex max-w-lg rounded-md shadow-sm'>
                    <input
                      type='text'
                      name='url'
                      id='url'
                      value={url}
                      onChange={(e) => handleChange(e)}
                      className='block w-full min-w-0 flex-1 rounded-none rounded-r-md border-0 py-1.5 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6'
                    />
                    <button onClick={(e) => submitForm(e)} type='button' className='  px-2 py-1.5 text-sm font-medium'>
                      Submit Url
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      {notification.status && <Alert title='Error' message={notification.message} status={notification.status} />}
    </div>
  );
}

import { useState, useEffect } from 'react';

//Components
import Alert from '../Alert';

//Types
import { UrlModel } from '../../../../core/models/Url.Models';
import NotificationModel from '../../../../core/models/Notification.Model';
interface ListUrlsPropsModel {
  renderKey: number;
}

//Icons
import { FaceFrownIcon, FaceSmileIcon } from '@heroicons/react/20/solid';

export default function ListUrls(props: ListUrlsPropsModel) {
  const [urls, setUrls] = useState<UrlModel[]>([]);
  const [notification, SetNotification] = useState<NotificationModel>({ status: '', message: '' });

  const renderKey = props.renderKey;

  useEffect(() => {
    fetchUrls();
  }, [renderKey]);

  const fetchUrls = () => {
    fetch(`${process.env.REACT_APP_API_URL}/getUrls`)
      .then((response) => response.json())
      .then((result) => {
        setUrls(result.data.urls);
      })
      .catch(() => {
        SetNotification({ status: 'error', message: 'Error while fetching the urls' });
      });
  };

  return (
    <div>
      {urls.length > 0 && (
        <div>
          <div>
            <h2 className='text-base font-semibold leading-6 text-gray-900'>List</h2>
            <p className='mt-1 max-w-2xl text-sm text-gray-500'>Here is a list of URLs you have entered previously.</p>
          </div>
          <div className='mt-8 flow-root'>
            <div className='-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8'>
              <div className='inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8'>
                <table className='min-w-full divide-y divide-gray-300'>
                  <thead>
                    <tr>
                      <th scope='col' className='py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0'>
                        URL
                      </th>
                      <th scope='col' className='py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0'>
                        Valid
                      </th>
                      <th scope='col' className='py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-0'>
                        Details
                      </th>
                    </tr>
                  </thead>
                  {
                    <tbody className='divide-y divide-gray-200'>
                      {urls.map((url) => (
                        <tr key={url._id}>
                          <td className='whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0'>{url.link}</td>
                          <td className='whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0'>
                            {url.notices.length > 0 ? (
                              <FaceFrownIcon className='h-5 w-5 text-red-400' aria-hidden='true' />
                            ) : (
                              <FaceSmileIcon className='h-5 w-5 text-green-400' aria-hidden='true' />
                            )}
                          </td>
                          <td style={{ maxWidth: 300 }} className=' py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-0'>
                            {url.notices.length > 0 && (
                              <ul>
                                {url.notices.map((e, i) => (
                                  <li key={i}>{e}</li>
                                ))}
                              </ul>
                            )}
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  }
                </table>
              </div>
            </div>
          </div>
          {notification.status && <Alert title='Error' message={notification.message} status={notification.status} />}
        </div>
      )}
      {urls.length === 0 && (
        <div>
          <p className='mt-1 max-w-2xl text-sm text-gray-500'>No Urls ... yet !</p>
        </div>
      )}
    </div>
  );
}

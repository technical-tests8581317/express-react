import { FaceFrownIcon, FaceSmileIcon } from '@heroicons/react/20/solid';

interface AlertModel {
  title: string;
  message: string;
  status: string;
}

export default function Alert(props: AlertModel) {
  const message = props.message;
  const status = props.status;
  const title = status === 'error' ? 'Error' : 'Success';
  const color = status === 'error' ? 'red' : 'green';

  return (
    <div className={`rounded-md bg-${color}-50 p-4 my-4`}>
      <div className='flex'>
        <div className='flex-shrink-0'>
          {status === 'success' && <FaceSmileIcon className={`h-5 w-5 text-${color}-400`} aria-hidden='true' />}
          {status === 'error' && <FaceFrownIcon className={`h-5 w-5 text-${color}-400`} aria-hidden='true' />}
        </div>
        <div className='ml-3'>
          <h3 className={`text-sm font-medium text-${color}-800`}>{title}</h3>
          <div className={`mt-2 text-sm text-${color}-700`}>
            <p>{message}</p>
          </div>
        </div>
      </div>
    </div>
  );
}

interface ErrorModel {
  code: string;
  message: string;
}

const checkUrlFormat = (url: string) => {
  let error = {} as ErrorModel;
  let data = false;
  const regexUrl = /^(http[s]?:\/\/(www\.)?){1}([0-9A-Za-z-.@:%_+~#=]+)+((\.[a-zA-Z]{2,3})+)(\/(.)*)?(\?(.)*)?/gim;
  if (regexUrl.test(url)) {
    data = true;
  } else {
    error = {
      code: 'invalidUrl',
      message: 'Please insert a valid url.',
    };
  }

  const checkedUrl: [ErrorModel, boolean] = [error, data];

  return checkedUrl;
};
export default checkUrlFormat;

import './Root.css';
import { Layout } from '../layouts/Layout';
import { useState } from 'react';

//Components
import FormUrl from '../components/urlsValidation/Form.UrlsValidation';
import ListUrls from '../components/urlsValidation/List.UrlsValidation';
export default function Root() {
  const [renderKey, setRenderKey] = useState(0);

  const handleNewUrl = () => {
    setRenderKey(renderKey + 1);
  };

  return (
    <Layout header='URLs Validation'>
      <FormUrl newUrl={handleNewUrl} />
      <hr className='my-4' />
      <ListUrls renderKey={renderKey} />
    </Layout>
  );
}

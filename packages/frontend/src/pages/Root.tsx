import './Root.css';
import { Layout } from '../layouts/Layout';

export default function Root() {
  return (
    <Layout header='Home'>
      <p>Please check my challenge by clicking on URLs Validation in the header menu.</p>
    </Layout>
  );
}

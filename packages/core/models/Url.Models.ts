interface UrlModel {
	_id?: number;
	link: string;
	notices: string[];
}

export type { UrlModel };

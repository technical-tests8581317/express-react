interface ErrorModel {
	code: string;
	message: string;
}

export default ErrorModel;

interface NotificationModel {
	status: string;
	message: string;
}
export default NotificationModel;

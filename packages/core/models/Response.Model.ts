interface ResponseModel {
	data: {
		ok: boolean;
		error: string;
		body: unknown;
	};
}

export default ResponseModel;

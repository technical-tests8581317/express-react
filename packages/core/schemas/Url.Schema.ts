const mongoose = require('mongoose');
import { Schema } from 'mongoose';

const UrlSchema = mongoose.model(
	'Url',
	new Schema({
		_id: { type: String, required: true },
		link: { type: String, required: true },
		notices: { type: Array }
	})
);

export default UrlSchema;

//Types
import UrlService from '../services/Url.Service';

//Types
import { UrlModel } from '../../../core/models/Url.Models';

//Axios
import axios from 'axios';

const UrlController = {
	//Get the list of urls in the database
	async getUrls(): Promise<[UrlModel] | Error> {
		try {
			const response = await UrlService.getUrls();
			return response;
		} catch (error) {
			return new Error();
		}
	},

	//Add an Url in the database
	async addurl(url: string): Promise<UrlModel | Error | unknown> {
		try {
			let notices = [] as string[];

			axios
				.get(url)
				.then(async function (response: any) {
					//1 - Check Status
					await UrlService.checkUrlStatus(response);

					//2 - Check SSL Details
					const noticesSSL = <string[]>(
						await UrlService.checkSSL(url, notices)
					);
					if (noticesSSL) {
						notices = [...notices, ...noticesSSL];
					}

					//3 - Check XFrame
					notices = <string[]>(
						await UrlService.checkXFrame(response.headers, notices)
					);

					//4 - Check Transport Security
					notices = <string[]>(
						await UrlService.checkTransportSecurity(
							response.headers,
							notices
						)
					);

					//5 - Check Content Security Policty
					notices = <string[]>(
						await UrlService.checkContentSecurityPolicy(
							response.headers,
							notices
						)
					);

					//6 - Proceed to saving the url in the DB
					const saveUrlResponse = await UrlService.saveUrl(
						url,
						notices
					);
					if (saveUrlResponse) {
						const newUrl: UrlModel = {
							link: url,
							notices
						};
						return newUrl;
					} else {
						throw new Error('Cannot save url');
					}
				})
				.catch(function (error) {
					throw new Error(error.message);
				});
		} catch (error: unknown) {
			if (error instanceof Error) {
				return new Error(error.message);
			} else if (typeof error === 'string') {
				return new Error(error);
			}
		}
	}
};
export default UrlController;

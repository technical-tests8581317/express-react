const mongoose = require('mongoose');
import UrlSchema from '../../../core/schemas/Url.Schema';
import { UrlModel } from '../../../core/models/Url.Models';

//SSL Checker
import sslChecker from 'ssl-checker';

//Axios
import { AxiosRequestHeaders } from 'axios';

const UrlServices = {
	async getUrls(): Promise<[UrlModel] | Error> {
		try {
			const urls = <[UrlModel]>await UrlSchema.find();
			return urls;
		} catch (error) {
			return new Error();
		}
	},

	//Check URL Status
	async checkUrlStatus(response: any): Promise<boolean | Error> {
		if (response.status !== 200) {
			throw new Error(
				'Cannot access to website. Please try another one.'
			);
		}
		return true;
	},

	//Check SSL Details
	async checkSSL(url: string, notices: string[]): Promise<string[] | Error> {
		try {
			await new Promise(async (resolve, reject) => {
				const domain = url
					.replace(/^(?:https?:\/\/)?(?:www\.)?/i, '')
					.split('/')[0];
				resolve(true);

				const sslDetails: any = await sslChecker(domain);

				//Check the address of the website
				if (
					sslDetails !== undefined &&
					sslDetails.syscall &&
					sslDetails.syscall === 'getaddrinfo'
				) {
					reject('Cannot access to website. Please try another one.');
				}
				//Then let's check if the ssl is valid
				else {
					if (sslDetails !== undefined && !sslDetails.valid) {
						notices.push('This website does not have a Valid SSL.');
						resolve(true);
					} else {
						reject(
							'Cannot access to website. Please try another one.'
						);
					}
				}
			});
			return notices;
		} catch (error) {
			return new Error();
		}
	},

	//Check XFrame Options
	async checkXFrame(
		headers: AxiosRequestHeaders,
		notices: string[]
	): Promise<string[] | Error> {
		try {
			const xFrameOptions = headers['x-frame-options'];
			if (xFrameOptions !== 'SAMEORIGIN' && xFrameOptions !== 'DENY') {
				notices.push(
					'This website is not secured against Clickjacking ! XFrameOptions must be SAMEORIGIN or DENY.'
				);
			}
			return notices;
		} catch (error) {
			return new Error();
		}
	},

	//Check Transport Security
	async checkTransportSecurity(
		headers: AxiosRequestHeaders,
		notices: string[]
	): Promise<string[] | Error> {
		try {
			const hsts = headers['strict-transport-security'];
			if (!hsts || (hsts && !hsts.includes('max-age='))) {
				notices.push(
					'This website is not configured for being accessed only through HTTPS.'
				);
			}
			return notices;
		} catch (error) {
			return new Error();
		}
	},

	//Check Content Security Policy
	async checkContentSecurityPolicy(
		headers: AxiosRequestHeaders,
		notices: string[]
	): Promise<string[] | Error> {
		try {
			const contentSecurityPolicy = headers['content-security-policy'];
			if (!contentSecurityPolicy) {
				notices.push(
					'This website does not have a Content Security Policy.'
				);
			}
			return notices;
		} catch (error) {
			return new Error();
		}
	},

	//Save URL
	async saveUrl(url: string, notices: string[]): Promise<boolean | Error> {
		try {
			const _id = new mongoose.Types.ObjectId();
			const newurl = { _id, link: url, notices };
			await new UrlSchema(newurl).save();
			return true;
		} catch (error) {
			return new Error();
		}
	}
};
export default UrlServices;

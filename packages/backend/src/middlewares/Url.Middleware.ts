//Models
import { Request, Response, NextFunction } from 'express';
import checkUrlFormat from '../functions/checkUrlFormat';
const UrlMiddleware = () => {
	return async (req: Request, res: Response, next: NextFunction) => {
		try {
			//Step 1 - Check if we have an url
			if (!req.body.url) {
				throw new Error('You must provide an url !');
			}
			const url: string = req.body.url;

			//Step 2 - Check the url format
			const [errorFormat, validFormat] = checkUrlFormat(url);
			if (errorFormat && !validFormat) {
				throw new Error('The format of the url is not valid !');
			}

			next();
		} catch (error: unknown) {
			if (error instanceof Error) {
				res.status(403).send({
					ok: false,
					error: error.message
				});
			} else {
				res.status(403).send({
					ok: false,
					error: 'Something wrong happened'
				});
			}
		}
	};
};

export default UrlMiddleware;

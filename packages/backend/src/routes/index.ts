import { NextFunction, Request, Response } from 'express';
const router = require('express').Router();
import checkUrl from '../middlewares/Url.Middleware';
import UrlController from '../controllers/Url.Controller';
import { UrlModel } from '../../../core/models/Url.Models';

//Check Health Status
router.get('/health', (req: Request, res: Response, next: NextFunction) => {
	res.locals.data = {
		healthy: true
	};
	next();
});

//Check Url
router.post(
	'/checkUrl',
	checkUrl(),
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			let isSecured = true;
			const newUrl = <UrlModel>await UrlController.addurl(req.body.url);
			if (newUrl instanceof Error) {
				throw new Error(newUrl.message);
			}
			if (newUrl !== undefined && newUrl.notices.length > 0)
				isSecured = false;
			res.locals.data = {
				isSecured
			};
			next();
		} catch (e) {
			next(e);
		}
	}
);

//Get Urls
router.get(
	'/getUrls',
	async (req: Request, res: Response, next: NextFunction) => {
		try {
			const urls = <[UrlModel]>await UrlController.getUrls();
			res.locals.data = {
				urls
			};
			next();
		} catch (e) {
			next(e);
		}
	}
);

//Form Result
router.use((req: Request, res: Response, next: NextFunction) => {
	const body = {
		ok: true,
		...res.locals
	};
	if (!res.locals.data) {
		throw new Error('No data received from your endpoint');
	}
	res.status(200).send(body);
	next();
});

module.exports = router;

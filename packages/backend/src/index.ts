import express from 'express';
import { MongoClient } from 'mongodb';
import * as dotenv from 'dotenv';
import mongoose from 'mongoose';
import pino from 'pino-http';
import { logger } from './logger';
import { NextFunction, Request, Response, ErrorRequestHandler } from 'express';

const MONGO_URL =
	process.env.MONGO_URL ?? 'mongodb://localhost:27017/hschallenge';
const cors = require('cors');
dotenv.config();

async function main() {
	try {
		logger.info(`Running as app`);

		mongoose.connect(MONGO_URL);
		mongoose.connection.on('connected', () => {
			logger.info('Connected to mongodb');
		});

		mongoose.connection.on('error', (err) => {
			logger.error(err, 'Mongodb connection error');
			process.exit(1);
		});

		const mongoClient = new MongoClient(MONGO_URL);
		await mongoClient.connect();

		const app = express();
		app.use(
			cors({
				origin: process.env.APP_FRONT,
				methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH']
			})
		);
		app.use(
			express.json({
				limit: '1mb'
			})
		);

		app.use(express.urlencoded({ limit: '1mb', extended: false }));

		app.use(
			pino({
				logger
			})
		);

		//Routes
		app.use('/', require('./routes'));

		//Error Handling
		app.use(function (
			err: Error,
			req: Request,
			res: Response,
			next: NextFunction
		) {
			const status = res.statusCode || 503;
			res.status(status).send({
				ok: false,
				error: err.message
			});
		});

		app.listen(process.env.PORT, () => {
			logger.info(`Backend server listening on port ${process.env.PORT}`);
		});
	} catch (err) {
		logger.error(err, 'Failed to start app');
	}
}

main();
